# Fake TeeSpring API

Use to test TeeSpring messaging with the teespring engine.

- Start api: `ruby api.rb`
- Use the FakeTeespring Postman collection to test the API
  - Runs on default port, <localhost:4567>
- Use the TeeSringEngine Postman collection to test peeps
  - Use console to advance order steps

## Fulfillment Workflow Messages

`PATCH /api/jobs/{job_id}`

- Receive job "accepted" message

`GET /api/shipments/{shipper_reference}/label`

- Returns a shipping label for a shipment
  - Uses `shipper_reference` identifier


## TeeSpring API

- URI <https://ppapi-sandbox.herokuapp.com/fulfillment/>
- Docs <https://ppapi-sandbox.herokuapp.com/fulfillment/docs#/>
